# MQTT

A go-lang based simple to use mqtt server.

## Docker 

take a look hear <https://hub.docker.com/repository/docker/ludal/mqtt>

or run:

```bash
docker run -P ludal/mqtt
```