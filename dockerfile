FROM golang:1.18 AS builder
WORKDIR /build

COPY go.mod .
COPY go.sum .
COPY . .


COPY *.go .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o /mqtt .
#RUN go install
EXPOSE 1883
CMD [ "/mqtt" ]

FROM alpine:3.16
COPY --from=builder /mqtt /
CMD [ "/mqtt" ]
