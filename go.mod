module example.com/m/v2

go 1.17

require github.com/mdzio/go-mqtt v0.1.5

require (
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/mdzio/go-logging v1.0.0 // indirect
)
