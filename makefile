v=0.4

all: push bins

docker:
	docker build -t ludal/mqtt:$v -t ludal/mqtt:latest .
push: docker
	docker push ludal/mqtt:$v
	docker push ludal/mqtt:latest
bins:
	GOOS=windows GOARCH=amd64 go build -o mqtt-windows-amd64.exe
	GOOS=windows GOARCH=386 go build -o mqtt-windows-386.exe
	GOOS=linux GOARCH=386 go build -o mqtt-linux-386
	GOOS=linux GOARCH=amd64 go build -o mqtt-linux-amd64
	GOOS=linux GOARCH=arm go build -o mqtt-linux-arm
	GOOS=linux GOARCH=arm go build -o mqtt-linux-arm64
	GOOS=darwin GOARCH=arm64 go build -o mqtt-darwin-arm64
	GOOS=darwin GOARCH=amd64 go build -o mqtt-darwin-amd64
	zip -m MQTT-v$v-multi.zip mqtt-*
version:
	echo v$v
