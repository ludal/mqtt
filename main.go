package main

import (
	"fmt"
	"log"

	"github.com/mdzio/go-mqtt/service"
)

var port = 1883

func main() {
	// Create a new server
	svr := &service.Server{
		KeepAlive:        300,           // seconds
		ConnectTimeout:   120,           // seconds
		SessionsProvider: "mem",         // keeps sessions in memory
		Authenticator:    "mockSuccess", // always succeed
		TopicsProvider:   "mem",         // keeps topic subscriptions in memory
	}

	// Listen and serve connections at localhost:1883
	s := fmt.Sprintf("tcp://:%d", port)
	log.Printf("mqtt starting on %s \n", s)
	svr.ListenAndServe(s)
}
